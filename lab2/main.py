'''
# 1.შეიტანეთ ათწილადი რიცხვი, დაამრგვალეთ ათწილად ნაწილში მეათედის სიზუსტით (1 ციფრი ათწილად ნაწილში) და დაბეჭდეთ
# შედეგი. გამოიყენეთ round, ceil, floor, trunc ფუნქციები სათითაოდ და შეამოწმეთ შედეგი თითოეულის გამოყენებით.
import math
num=float(input("Enter a number: "))

num_round=round(num, 1)
print(f"using round: {num_round}")

num_ceil=math.ceil(num*10)/10
print(f"using ceil: {num_ceil}")

num_floor=math.floor(num*10)/10
print(f"using floor: {num_floor}")

print(f"using trunc: {math.trunc(num)}")


# 2.შეიტანეთ სამი რიცხვი, იპოვეთ მათ შორის მაქსიმუმი და დაბეჭდეთ შედეგი.
# გამოიყენეთ max ფუნქცია.
num1=input("Ricxvi 1: ")
num2=input("Ricxvi 2: ")
num3=input("Ricxvi 3: ")

maximum=max(num1, num2, num3)
print(f"The maximum number is: {maximum}")


# 3.გამოთვალეთ შემდეგი გამოსახულების შედეგი ფუნქციის გამოყენებით:
#  ა)√225625       ბ)√4225
import math

def square_root(number):
    return math.sqrt(number)

a=square_root(225625)
print("Square root of 225625 is: ", a)

b=square_root(4225)
print("Square root of 4225 is: ", b)


# 4.დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი დიაპაზონიდან 0-დან 1-ის ჩათვლით. დაამრგვალეთ რიცხვი
# (3 ციფრი ათწილად ნაწილში) და დაბეჭდეთ.
import random
random_decimal=random.uniform(0, 1)

rounded_decimal=round(random_decimal, 3)
print("Random rounded decimal number:", rounded_decimal)


# 5.დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი 100-დან 120-მდე. დაამრგვალეთ რიცხვი (1 ციფრი ათწილად ნაწილში)
# და ისე გამოიტანეთ.
import random
random_decimal=random.uniform(100, 120)
rounded_decimal=round(random_decimal, 1)
print("Random rounded decimal number:", rounded_decimal)


# 6.დააგენერირეთ 10 შემთხვევითი მთელი რიცხვი და დაბეჭდეთ ეკრანზე. მითითება:
# გამოიყენეთ ციკლის ოპერატორი.
import random
random_numbers=[random.randint(1,100) for _ in range(10)]
print(random_numbers)


#7.შექმენით ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის (დააბრუნებს) მათ საშუალო არითმეტიკულს.
# გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის და დაბეჭდეთ შედეგი.
def calculate_average(num1, num2):
    return (num1+num2)/2

res1=calculate_average(4, 1)
print("Arithmetic mean of 4 and 1:", res1)
res2=calculate_average(44, 13)
print("Arithmetic mean of 44 and 13:", res2)
res3=calculate_average(53, 35)
print("Arithmetic mean of 53 and 35:", res3)


# 8.დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის
#მათ საშუალო არითმეტიკულს და დაბეჭდავს შედეგს (გაითვალისწინეთ რომ
#დაბეჭდვა უნდა მოხდეს ფუნქციის შიგნით - ფუნქცია არ აბრუნებს
#მნიშვნელობას). გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის.
def calculate_average(num1, num2):
    average=(num1+num2)/2
    print(average)

calculate_average(10, 20)
calculate_average(7.5, 12)
calculate_average(-8, 88)

#9.	შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) არგუმენტად გადაცემული რიცხვის კუბს. გამოიძახეთ ფუნქცია
# რამდენიმეჯერ და დაბეჭდეთ მიღებული შედეგი.
def cube(number):
    return number**3

res1=cube(2)
print("Cube of 2:", res1)
res2=cube(5)
print("Cube of 5:", res2)
res3=cube(-2)
print("Cube of -2:", res3)

#10.შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) ორ რიცხვს შორის მინიმალურ მნიშვნელობას. გამოიძახეთ ფუნქცია
# და დაბეჭდეთ შედეგი. (პარამეტრად გადაეცით ნებისმიერი ორი რიცხვი).
def minimum(num1, num2):
    return min(num1, num2)

min=minimum(7, 23)
print("Minimum value between 7 and 23 is:", min)

#11.დაწერეთ ფუნქცია, რომელიც შეამოწმებს პარამეტრად გადაცემული რიცხვი არის თუ არა კენტი. თუ კენტია,
# დააბრუნოს მნიშვნელობა True, თუ არადა - False. შეამოწმეთ რამდენიმე რიცხვისთვის და დაბეჭდეთ შედეგი.
def is_odd(number):
    return number % 2 != 0

numbers_to_check = [3, 8, 11, 16, 21]
for number in numbers_to_check:
    result=is_odd(number)
    print(f"is odd:  {result}")

#12.დაწერეთ ფუნქცია, რომელიც დაითვლის (დააბრუნებს) პარამეტრად გადაცემული რიცხვის ფაქტორიალს და დაბეჭდეთ
# შედეგი სხვადასხვა რიცხვებისთვის.
def factorial(number):
    if number < 0:
        return "Factorial is not defined for negative numbers"
    elif number == 0:
        return 1
    else:
        result = 1
        for i in range(1, number+1):
            result *= i
        return result
numbers = [5, 7, 10, -3, 0]

for num in numbers:
    print(f"The factorial of {num} is {factorial(num)}")

#13.დაწერეთ უპარამეტრო ფუნქცია რომელიც ეკრანზე ბეჭდავს შემდეგ ტექსტს: “Hello World”.
# (გაითვალისწინეთ რომ ფუნქცია არ აბრუნებს მნიშვნელობას).
def hello_world():
    print("Hello World")
hello_world()

#14.დაწერეთ ანონიმური ფუნქცია რომელიც დაითვლის რიცხვის კუბს.
cube = lambda x: x**3

result=cube(5)
print(result)

#15.შექმენით სია numbs ნებისმიერ 5 რიცხვითი მნიშვნელობით. იპოვეთ ამ რიცხვების ჯამი, მინიმალური, მაქსიმალური და
# საშუალო არითმეტიკული. ასევე შეასრულეთ შემდეგი ოპერაციები:
# სიას დაამატეთ ბოლო ელემენტად რიცხვი 102
# სიის მესამე ელემენტად ჩასვით რიცხვი 205
# წაშალეთ სიის მე-4 ელემენტი
numbers=[10, 15, 20, 15, 30]

sum_numbers = sum(numbers)
min_number = min(numbers)
max_number = max(numbers)
mean = sum_numbers / len(numbers)

print(f"Original List: {numbers}")
print(f"Sum: {sum_numbers}")
print(f"Minimum: {min_number}")
print(f"Maximum: {max_number}")
print(f"Arithmetic Mean: {mean:.2f}")

numbers.append(102)

numbers.insert(2, 205)

del numbers[3]

sum_numbers = sum(numbers)
min_number = min(numbers)
max_number = max(numbers)
mean = sum_numbers / len(numbers)

print(f"Updated List: {numbers}")
print(f"Updated Sum: {sum_numbers}")
print(f"Updated Minimum: {min_number}")
print(f"Updated Maximum: {max_number}")
print(f"Updated Arithmetic Mean: {mean:.2f}")

#16.დაწერეთ პროგრამა, რომლის მეშვეობით შეიყვანთ (input-ით) 10 მონაცემს. წარმოადგინეთ და დაბეჭდეთ ისინი
# სიის ელემენტების სახის.
list = []
for i in range(10):
    data = input(f"Enter data {i + 1}: ")
    list.append(data)

print("List:")
for item in list:
    print(item)

#17.შექმენით სია fruits, რომელის ელემენტებია: Watermelon, Banana, Apple. დაალაგეთ სიის ელემენტები ალფაბეტის
# უკუ-მიმართულებით და დაბეჭდეთ ისინი.
fruits = ["Watermelon", "Banana", "Apple"]
fruits.sort(reverse=True)
for fruit in fruits:
    print(fruit)

#18.დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა სია, დაითვლის სიის ელემენტების ნამრავლს და დააბრუნებს შედეგს.
# გამოიძახეთ ფუნქცია ნებისმიერი სიისთვის.
def elements(input_list):
 result = 1
 for element in input_list:
  result *= element
 return result

my_list = [2, 3, 4, 5]
result = elements(my_list)
print(f"The product of elements in the list is: {result}")

#19.დაწერეთ პროგრამა, რომელიც რიცხვითი მნიშვნელობების სიაში ამოშლის კენტ რიცხვებს. დაბეჭდეთ მიღებული სია.
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_numbers = [x for x in numbers if x % 2 == 0]
print("List with odd numbers removed:")
print(even_numbers)

#20.შექმენით 5 ელემენტიანი სია (რიცხვითი მნიშვნელობებით). თითოეული ელემენტი გაზარდეთ 10-ით და დაბეჭდეთ სია.
list = [2, 4, 6, 8, 10]
modified_list = [x + 10 for x in list]

print("Modified List:")
print(modified_list)

#21.შექმენით 10 ელემენტიანი სია, რომლის ელემენტებია ნებისმიერი შემთხვევითი მთელი რიცხვები 25-დან 110-მდე.
# დაბეჭდეთ სია და იპოვეთ მინიმალური ელემენტი.
import random
random_list = [random.randint(25, 110) for _ in range(10)]
print("Random List:")
print(random_list)
min_element = min(random_list)
print(f"The minimal element in the list is: {min_element}")

#22.დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა 2 სია. ფუნქცია აბრუნებს მნიშვნელობა True-ს თუ სიებს აქვთ
# ერთი მაინც საერთო ელემენტი. წინააღმდეგ შემთხვევაში აბრუნებს False მნიშვნელობას.
def have_common_element(list1, list2):
 for item in list1:
  if item in list2:
   return True
 return False
list1 = [1, 2, 3, 4, 5]
list2 = [0, 6, 7, 8, 9]
result = have_common_element(list1, list2)
print(result)

#23.დაწერეთ ფუნქცია, რომელიც სიაში არსებულ კენტ რიცხვებს წაშლის. ფუნქცია აბრუნებს განახლებულ სიას.
def remove_odd_numbers(input_list):
 even_numbers = [x for x in input_list if x % 2 == 0]
 return even_numbers
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
result = remove_odd_numbers(my_list)
print(result)

#24.შექმენით სია რიცხვითი ელემენტებით. shuffle ფუნქციის გამოყენებით (random მოდულიდან) მოახდინეთ სიის ელემენტების
# შემთხვევითად არევა და დაბეჭდეთ მიღებული სია. (მითითება: ფუნქცია იწერება შემდეგნაირად: random.shuffle(x)
# სადაც x სიის დასახელებაა)
import random
def shuffle_list(input_list):
 random.shuffle(input_list)

numeric_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
shuffle_list(numeric_list)
print("Shuffled List:")
print(numeric_list)

#25.შექმენით სია რიცხვითი მნიშვნელობებით. რანდმულად ამოარჩიეთ სიის რომელიმე ელემენტი და დაბეჭდეთ. (მითითება: წინა
# სავარჯიშოს მსგავსად გამოიყენეთ random მოდულის choice ფუნქცია).
import random
numeric_list = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
random_item = random.choice(numeric_list)
print("Randomly Selected Item:")
print(random_item)

#26.დაწერეთ პროგრამა, რომელშიც შეიტანთ (input-ით) ნებისმიერ დიდ რიცხვს (მაგ. 342387410984). იპოვეთ რიცხვის ციფრთა ჯამი.
# (მითითება: თავდაპირველად გარდაქმენით რიცხვი სიად).
large_number_str = input("Enter a large number: ")
sum_of_digits = 0
digits_list = [int(digit) for digit in large_number_str if digit.isdigit()]
for digit in digits_list:
    sum_of_digits += digit
print(f"The sum of the digits of the number is: {sum_of_digits}")

#27.იპოვეთ სიაში [1, 5, 23, 5, 12, 2, 5, 1, 18, 5] ყველაზე ხშირად განმეორებადი რიცხვი. დაბეჭდეთ შედეგი.
# ასევე მიუთითეთ რამდენჯერ შეგხვდათ სიაში ყველაზე ხშირად განმეორებადი რიცხვი.
from collections import Counter
my_list = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]
number_counts = Counter(my_list)
most_common_number, count = number_counts.most_common(1)[0]
print(f"The most frequently occurring number is {most_common_number}")
print(f"It occurs {count} times in the list.")

#28.შექმენით სია extensions = ['txt', 'jpg', 'gif', 'html']. პროგრამის გაშვების შემდეგ მომხამრებელმა შეიყვანოს (input)
# ნებისმიერი ფაილის დასახელება. თუ ფაილის გაფართოება ემთხევა სიის რომელიმე ელემენტს, დაბეჭდოს ეკრანზე “Yes”, წინააღმდეგ
# შემთხვავაში დაბეჭდოს “No”.
extensions = ['txt', 'jpg', 'gif', 'html']
file_name = input("Enter the file name: ")
file_extension = file_name.split('.')[-1]
if file_extension in extensions:
    print("Yes")
else:
    print("No")

#29.სტრიქონი 'python php pascal javascript java c++' წარმოადგინეთ სიის სახით (სტრიქონის თითოეული სიტყვა სიის თითოეული
# ელემენტად). იპოვეთ სიის ყველაზე გრძელი ელემენტი (ანუ ყველაზე გრძელი სიტყვა).
input_string = 'python php pascal javascript java c++'
word_list = input_string.split()
longest_word = max(word_list, key=len)
print(word_list)
print("Longest word in the list:", longest_word)

#30.შეიტანეთ სიის 10 ელემენტი. იპოვეთ ამ რიცხვების საშუალო არითმეტიკული, მედიანა და მოდა. გაითვალისწინეთ, მედიანა
# წარმოადგენს შუა ელემენტს, როდესაც რიცხვები დალაგებულია ზრდადობით (ან კლებადობით); თუ შუაში ორი ელემენტია, მაშინ
# მედიანა არის ამ შუა ელემენტების საშუალო არითმეტიკული. მოდა, არის მიმდევრობაში რიცხვი, რომელიც ყველაზე ხშირად
# გვხვდება. შეიძლება მიმდევრობას არ ქონდეს მოდა (თუ ყველა ელემენტს ერთნაირი სიხშირე აქვს), ან ქონდეს ერთი ან
# რამდენიმე მოდა.
from statistics import mean, median, mode
numbers = []
for i in range(10):
    num = float(input(f"Enter number {i + 1}: "))
    numbers.append(num)

mean_value = mean(numbers)
median_value = median(numbers)
mode_value = mode(numbers)

print(f"Arithmetic Mean: {mean_value:.2f}")
print(f"Median: {median_value:.2f}")
print(f"Mode: {mode_value}")
'''













