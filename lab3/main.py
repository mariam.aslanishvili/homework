'''

#2. 0-დან 100-მდე მთელი რიცხვები ჩაწერეთ data1.txt ფაილში. myFiles
#საქაღალდეში.
import os
directory_path = "myFiles"

if not os.path.exists(directory_path):
    os.makedirs(directory_path)

file_path = os.path.join(directory_path,"data1.txt")

with open(file_path, "w") as file:
    for i in range(2, 101):
        file.write(str(i) + "\n")

#4.myFiles2 საქაღალდეში შექმენით 30 ფაილი, ფაილებში ჩაწერეთ სიტყვები
#„Programmer1“, „Programmer2“ .... „Programmer30“.
import os
directory_path = "myFiles2"

if not os.path.exists(directory_path):
    os.makedirs(directory_path)

for i in range(1, 31):
    file_path = os.path.join(directory_path, f"file{i}.txt")
    with open(file_path, "w") as file:
        file.write(f"Programmer{i}\n")

#7.დაწერეთ პროგრამა, რომელიც ფუნქციის მნიშვნელობებს დაითვლის [0; 2 ]
#შუალედში მეასედების სიზუსტით და შესაბამის მნიშვნელობებს ჩაწერს
#function.txt ფაილში myFiles საქაღალდეში.
import os

directory_path = "myFiles"

if not os.path.exists(directory_path):
    os.makedirs(directory_path)

file_path = os.path.join(directory_path, "function.txt")

start = 0.0
end = 2.01
accuracy = 0.01

with open(file_path, "w") as file:
    x = start
    while x <= end:
        file.write(f"{x:.2f}\n")
        x += accuracy

#ელემენტი.
my_dict = {0: 10, 1: 20}

my_dict[2]=30
my_dict[3]=40

#10. შექმენით ლექსიკონი: {0: 10, 1: 20}. დაამატეთ 2 ახალი ელემენტი და დაბეჭდეთ
#მიღებული ლექსიკონი. (გამოიყენეთ update მეთოდიც). წაშალეთ რომელიმე
print(my_dict)

if 1 in my_dict:
    del my_dict[1]

print(my_dict)

#12. დაწერეთ პროგრამა რომელიც შეამოწმებს რომელიმე key (გასაღები) არის თუ
#არა ლექსიკონში: d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60} და დაბეჭდეთ
#შესაბამისი შეტობინება. (მითითება: გამოიყენეთ in ოპერატორი).
d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}

key_to_check = 7

if key_to_check in d:
    print(f"key {key_to_check} is in the dictionary and its value is {d[key_to_check]}")
else:
    print(f"key {key_to_check} isn't in the dictionary")

#18. შექმენით tuple ტიპის ობიექტი, რომლის ელემენტებია 1-დან 100-მდე
#არსებული 5-ის ჯერადი რიცხვების კუბები. გამოიყენეთ for ციკლის მოკლე
#ჩაწერის ფორმა. შესაბამისი ფუნქციის გამოყენებით იპოვეთ tuple-ის სიგრძე
#(ელემენტების რაოდენობა).
list = [i**3 for i in range(1, 101) if i%5==0]
tuple = tuple(list)
tuple_length = len(tuple)
print("Tuple: ", tuple)
print("Length of tuple: ", tuple_length)

'''








